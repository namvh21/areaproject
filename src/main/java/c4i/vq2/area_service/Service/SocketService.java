package c4i.vq2.area_service.Service;

import c4i.vq2.area_service.Model.Area;
import com.google.gson.Gson;
import io.socket.client.IO;
import io.socket.client.Socket;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.net.URI;
import java.util.List;

@Service
public class SocketService {
    @Value("${socket.server}")
    private String socketServer ;
    private Socket socket = IO.socket(URI.create("http://10.61.53.202:6969"));

    @PostConstruct
    private void postConstruct(){

//        this.socket = IO.socket(URI.create(socketServer)).connect();
    }

    public void socketPostRestApi(Area area)  {
        try{
            JSONObject object = new JSONObject(new Gson().toJson(area));
            socket.emit("create-area",object);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
    public void socketPutRestApi(Area area)  {
        try{
            JSONObject object = new JSONObject(new Gson().toJson(area));
            socket.emit("update-area",object);
            System.out.println(object);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
    public void socketDeleteRestApi(List<Area> areas) {
        try {
            JSONArray array = new JSONArray(new Gson().toJson(areas));
            socket.emit("delete-area", array);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
}
