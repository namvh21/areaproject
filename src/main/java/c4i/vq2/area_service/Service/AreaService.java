package c4i.vq2.area_service.Service;

import c4i.vq2.area_service.Model.Area;
import c4i.vq2.area_service.Repository.AreaRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Service
public class AreaService {

    @Autowired
    private AreaRepository areaRepository;

    @Autowired
    private SocketService socketService;

    public Area createArea(Area area)
    {
        Area newArea = area;
        newArea.setUpdateTime(null);
        newArea.setUpdateUser(null);
        newArea.setCreateTime(new Timestamp(System.currentTimeMillis()));
        if (area.getAreaName() == null)
        {
            newArea = null;
        }
        if (area.getCreateUser() == null)
        {
            newArea = null;
        }
        if ( newArea != null){
            areaRepository.save(newArea);
            socketService.socketPostRestApi(newArea);
        }
        return newArea;
    }
    public List<Area> findAllArea()
    {
        List<Area> areas = new ArrayList<Area>();
        areaRepository.findAll().forEach(areas::add);
        return areas;
    }
    public List<Area> findAreaByName(String areaName)
    {
        List<Area> areas = new ArrayList<Area>();
        areaRepository.findByAreaName(areaName).forEach(areas::add);
        return areas;
    }
    public Optional<Area> findAreaByID(UUID areaID)
    {
        Optional<Area> areas = areaRepository.findByAreaId(areaID);
        return areas;
    }
    public Area updateArea(UUID id, Area areas) {
        Optional<Area> searchArea = areaRepository.findByAreaId(id);
        Area newArea = null;
        if (searchArea.isPresent()) {
            newArea = searchArea.get();
            newArea.setAreaLatitude(areas.getAreaLatitude());
            newArea.setAreaLongitude(areas.getAreaLongitude());
            newArea.setAreaRadius(areas.getAreaRadius());
            newArea.setUpdateTime(new Timestamp(System.currentTimeMillis()));
            newArea.setUpdateUser(areas.getUpdateUser());
            newArea.setAreaName(areas.getAreaName());
            areaRepository.save(newArea);
        }
        if (newArea != null){
            socketService.socketPutRestApi(newArea);
        }
        return newArea; 
    }
    public void deleteAreaByName(String areaName){
        List<Area> areas= this.findAreaByName(areaName);
        if(areas.isEmpty()){}
        else {
            socketService.socketDeleteRestApi(areas);
        }
        areaRepository.deleteByAreaName(areaName);
    }
    public void deleteAllArea(){
        List<Area> areas  = this.findAllArea();
        if(areas.isEmpty()){}
        else {
            socketService.socketDeleteRestApi(areas);
        }
        areaRepository.deleteAll();
    }
    public void deleteAreaByID(UUID areaID){
        Optional<Area> areaFound = this.findAreaByID(areaID);
        if(areaFound.isPresent()){
            List<Area> areas = new ArrayList<Area>();
            areas.add(areaFound.get());
            socketService.socketDeleteRestApi(areas);
        }
        areaRepository.deleteByAreaId(areaID);
    }
}
