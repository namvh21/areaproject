package c4i.vq2.area_service.Service;

import c4i.vq2.area_service.Model.Employee;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;

@Service
public class RabbitMQSender {

    @Autowired
    private RabbitTemplate rabbitTemplate;

    @Value("${rabbitmq.exchange}")
    private String exchange;

    @Value("${rabbitmq.routingKey}")
    private String routingKey;

    @Value("${rabbitmq.queue}")
    private String queue;

    public void send(Employee company) {
        rabbitTemplate.convertAndSend(exchange, routingKey, "Test");
        System.out.println("Send msg = " + company);
    }
    @RabbitListener(queues = "track-queue")
    public void listen(String in) {
        System.out.println("Message read from myQueue : " + in);
    }

}