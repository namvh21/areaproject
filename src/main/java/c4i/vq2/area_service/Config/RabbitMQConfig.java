//package c4i.vq2.area_service.Config;
//
//import org.springframework.amqp.core.*;
//import org.springframework.amqp.rabbit.connection.ConnectionFactory;
//import org.springframework.amqp.rabbit.core.RabbitTemplate;
//import org.springframework.amqp.support.converter.Jackson2JsonMessageConverter;
//import org.springframework.amqp.support.converter.MessageConverter;
//import org.springframework.beans.factory.annotation.Value;
//import org.springframework.context.annotation.Bean;
//import org.springframework.context.annotation.Configuration;
//
//@Configuration
//public class RabbitMQConfig {
//    @Value("${rabbitmq.exchange}")
//    String exchange;
//    @Value("${rabbitmq.queue}")
//    String queue;
//    @Value("${rabbitmq.routingKey}")
//    String routingKey;
//    @Bean
//    DirectExchange exchange(){
//        return new DirectExchange(exchange);
//    }
//    @Bean
//    Binding binding(Queue queue, DirectExchange exchange){
//        return BindingBuilder.bind(queue).to(exchange).with(routingKey);
//    }
//    @Bean
//    Queue queue() {
//        return new Queue(queue, false);
//    }
//    @Bean
//    public MessageConverter jsonMessageConverter() {
//        return new Jackson2JsonMessageConverter();
//    }
//    public AmqpTemplate rabbitTemplate(ConnectionFactory connectionFactory) {
//        final RabbitTemplate rabbitTemplate = new RabbitTemplate(connectionFactory);
//        rabbitTemplate.setMessageConverter(jsonMessageConverter());
//        return rabbitTemplate;
//    }
//
//}
