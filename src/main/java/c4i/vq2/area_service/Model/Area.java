package c4i.vq2.area_service.Model;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.sql.Timestamp;
import java.util.UUID;
import javax.persistence.*;


@Entity
@Table(name = "areas")
@Getter
@Setter
@ToString
public class Area {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "area_id")
    private UUID areaId ;
    @Column(name = "create_time")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss.SSS Z")
    private Timestamp createTime;
    @Column(name = "update_time")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss.SSS Z")
    private Timestamp updateTime;
    @Column(name = "area_latitude")
    private double areaLatitude ;
    @Column(name = "area_longitude")
    private double areaLongitude;
    @Column(name = "create_user")
    private String createUser;
    @Column(name = "update_user")
    private String updateUser;
    @Column(name = "area_radius")
    private double areaRadius;
    @Column(name = "area_name")
    private String areaName;
}







