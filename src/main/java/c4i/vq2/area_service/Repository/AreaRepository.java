package c4i.vq2.area_service.Repository;

import c4i.vq2.area_service.Model.Area;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Repository
public interface AreaRepository extends CrudRepository<Area, Long> {
    List<Area> findByAreaName(String areaName);
    Optional<Area> findByAreaId(UUID areaId);
    @Transactional
    void deleteByAreaId(UUID areaId);
    @Transactional
    void deleteByAreaName(String areaName);
}
