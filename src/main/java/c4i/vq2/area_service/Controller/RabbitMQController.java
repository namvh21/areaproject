package c4i.vq2.area_service.Controller;

import c4i.vq2.area_service.Model.Employee;
import c4i.vq2.area_service.Service.RabbitMQSender;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;


@RestController
public class RabbitMQController {
    @Autowired
    RabbitMQSender rabbitMQSender;

    @GetMapping("/rabbitmq")
    public ResponseEntity<String> rabbitMQSend()
    {
        try{

            Employee emp=new Employee();
            emp.setEmpId("5");
            emp.setEmpName("Bad");
            rabbitMQSender.send(emp);
            return new ResponseEntity<>("Ok fine",HttpStatus.OK);
        }
        catch (Exception e)
        {
            return new ResponseEntity<>("Not ok",HttpStatus.NOT_FOUND);
        }
    }
}
