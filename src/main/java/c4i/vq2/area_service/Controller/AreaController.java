package c4i.vq2.area_service.Controller;

import c4i.vq2.area_service.Model.Area;
import c4i.vq2.area_service.Service.AreaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

@RestController
public class AreaController {
    @Autowired
    AreaService areaService;
    @CrossOrigin
    @PostMapping("/areas")
    public ResponseEntity<Area> apiCreateArea(@RequestBody Area area)
    {
        try
        {
            System.out.println("Try to create an area");
            Area newArea =  areaService.createArea(area);
            if (newArea != null){
                return new ResponseEntity<>(newArea, HttpStatus.CREATED);
            }
            else
                return new ResponseEntity<>(null,HttpStatus.BAD_REQUEST);

        }
        catch (Exception e)
        {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }

    }

    @CrossOrigin
    @GetMapping("/areas")
    public ResponseEntity<List<Area>> apiGetAllArea(@RequestParam(required = false) String areaName)
    {
        try{
            System.out.println("Try to get all area");
            List<Area> areas = new ArrayList<Area>();
            if(areaName == null )
                areaService.findAllArea().forEach(areas::add);
            else
                areaService.findAreaByName(areaName).forEach(areas::add);
            if(areas.isEmpty()) {
                return new ResponseEntity<>(HttpStatus.NO_CONTENT);
            }
            else
                return new ResponseEntity<>(areas, HttpStatus.OK);
        }
        catch (Exception e ){
            return new ResponseEntity<>(null,HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @CrossOrigin
    @GetMapping("/areas/{id}")
    public ResponseEntity<Area> apiGetAreaById(@PathVariable UUID id)
    {
        try{
            System.out.println("Try to get an area with "+ id);
            Optional<Area> areas = areaService.findAreaByID(id);
            if(areas.isPresent())
            {
                return new ResponseEntity<>(areas.get(),HttpStatus.OK);
            }
            else
                return new ResponseEntity<>(null,HttpStatus.NOT_FOUND);

        }
        catch (Exception e)
        {
            return new ResponseEntity<>(null,HttpStatus.NOT_FOUND);
        }
    }


    @CrossOrigin
    @PutMapping("/areas/{id}")
    public ResponseEntity<Area> apiUpdateArea(@PathVariable UUID id, @RequestBody Area areas)
    {
        System.out.print("Try to update an area ");
        Area newArea = areaService.updateArea(id,areas);
        if (newArea == null)
        {
            System.out.println("but not found");
            return new ResponseEntity<>(null,HttpStatus.NOT_FOUND);
        }
        else {
            System.out.println("with id " + areas.getAreaId());
            return new ResponseEntity<>(newArea,HttpStatus.OK);
        }

    }

    @CrossOrigin
    @DeleteMapping("/areas")
    public ResponseEntity<Area> apiDeleteAllArea(@RequestParam(required = false) String areaName)
    {
        try
        {
            if( areaName == null)
            {
                System.out.println("Try to delete all area");
                areaService.deleteAllArea();
            }
            else
            {
                System.out.println("Try to delete area with name"+ areaName);
                areaService.deleteAreaByName(areaName);
            }
            return new ResponseEntity<>(null,HttpStatus.NO_CONTENT);
        }
        catch (Exception e)
        {
            return new ResponseEntity<>(null,HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @CrossOrigin
    @DeleteMapping("/areas/{id}")
    public ResponseEntity<Area>  apiDeleteAreaById(@PathVariable UUID id)
    {
        try{
            System.out.println("Try to delete area"+id);
            areaService.deleteAreaByID(id);
            return new ResponseEntity<>(null,HttpStatus.NO_CONTENT);
        }
        catch (Exception e )
        {
            System.out.println(e);
            return new ResponseEntity<>(null,HttpStatus.INTERNAL_SERVER_ERROR);
        }

    }
}
